package com.ynlvko.steelkiwitest.utils;

public class Constants {
    public static final String BASE_SERVER_URL = "http://maps.googleapis.com/maps/api/geocode/json";

    public static final String PREFS = "Prefs";
    public static final String PREFS_LAST_SEARCH = "last_search";
}
