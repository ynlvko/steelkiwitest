package com.ynlvko.steelkiwitest.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;

public class Utils {
    public static void saveLastSearchRequest(Context context, String lastSearch) {
        SharedPreferences.Editor editor = context.getSharedPreferences(Constants.PREFS, 0).edit();
        editor.putString(Constants.PREFS_LAST_SEARCH, lastSearch);
        editor.commit();
    }

    public static String loadLastSearchRequest(Context context) {
        return context.getSharedPreferences(Constants.PREFS, 0).getString(Constants.PREFS_LAST_SEARCH, "");
    }

    public static String getString(Cursor c, String columnAddress) {
        return c.getString(c.getColumnIndex(columnAddress));
    }
}
