package com.ynlvko.steelkiwitest.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SKTDbHelper extends SQLiteOpenHelper {
    private static final String DB_NAME = "ta_db";
    private static final int DB_VERSION = 1;

    public SKTDbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Address.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + Address.TABLE_NAME + ";");

        onCreate(db);
    }
}
