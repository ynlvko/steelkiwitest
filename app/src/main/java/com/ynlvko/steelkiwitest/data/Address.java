package com.ynlvko.steelkiwitest.data;

import android.content.ContentValues;
import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Address extends BaseTable {
    public static final String TABLE_NAME = "addresses";

    public static final String COLUMN_ADDRESS = "formatted_address";

    public static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME
            + " ( "
            + COLUMN_ID + " INTEGER PRIMARY KEY, "
            + COLUMN_ADDRESS + " TEXT"
            + " );";

    public static ContentValues parseFromJson(JSONObject json) {
        ContentValues cv = new ContentValues();
        try {
            putString(COLUMN_ADDRESS, cv, json);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return cv;
    }

    public static void putToDb(Context context, JSONArray jsonArray) throws JSONException {
        ContentValues[] cvs = new ContentValues[jsonArray.length()];

        for (int i = 0; i < cvs.length; i++) {
            cvs[i] = parseFromJson(jsonArray.getJSONObject(i));
        }

        context.getContentResolver().bulkInsert(SKTContentProvider.CONTENT_ADDRESS_URI, cvs);
    }
}
