package com.ynlvko.steelkiwitest.data;

import android.content.ContentValues;

import org.json.JSONException;
import org.json.JSONObject;

public class BaseTable {
    public static final String COLUMN_ID = "_id";

    protected static void putString(String columnName, ContentValues cv, JSONObject json) throws JSONException {
        cv.put(columnName, json.has(columnName) ? json.getString(columnName) : "");
    }
}
