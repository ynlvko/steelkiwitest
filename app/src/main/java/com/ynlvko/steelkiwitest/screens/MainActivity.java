package com.ynlvko.steelkiwitest.screens;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Window;

import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.UncachedSpiceService;
import com.ynlvko.steelkiwitest.R;

public class MainActivity extends FragmentActivity {
    private SpiceManager mSpiceManager = new SpiceManager(UncachedSpiceService.class);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.activity_main);

        getSupportFragmentManager().beginTransaction().add(R.id.container, new MainFragment()).commit();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mSpiceManager.start(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mSpiceManager.shouldStop();
    }

    public SpiceManager getSpiceManager() {
        return mSpiceManager;
    }
}
