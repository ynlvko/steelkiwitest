package com.ynlvko.steelkiwitest.screens;

import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.ynlvko.steelkiwitest.R;
import com.ynlvko.steelkiwitest.adapters.AddressesAdapter;
import com.ynlvko.steelkiwitest.data.SKTContentProvider;
import com.ynlvko.steelkiwitest.network.SearchRequest;
import com.ynlvko.steelkiwitest.utils.Utils;

public class MainFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {
    private ListView mLvAddresses;
    private AddressesAdapter mAdapter;

    private EditText mEtSearch;
    private Button mBtnSearch;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_main, container, false);

        mAdapter = new AddressesAdapter(getActivity());
        mLvAddresses = (ListView) v.findViewById(R.id.lvAddresses);
        mLvAddresses.setAdapter(mAdapter);

        mEtSearch = (EditText) v.findViewById(R.id.etSearch);
        mEtSearch.setText(Utils.loadLastSearchRequest(getActivity()));

        mBtnSearch = (Button) v.findViewById(R.id.btnSearch);
        mBtnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.saveLastSearchRequest(getActivity(), mEtSearch.getText().toString());
                performRequest();
            }
        });

        getLoaderManager().initLoader(0, null, MainFragment.this);

        return v;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(getActivity(), SKTContentProvider.CONTENT_ADDRESS_URI, null, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor c) {
        mAdapter.changeCursor(c);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {
        mAdapter.changeCursor(null);
    }

    private void performRequest() {
        getActivity().setProgressBarIndeterminateVisibility(true);
        String url = mEtSearch.getText().toString();
        SearchRequest request = new SearchRequest(getActivity(), url, Void.class);

        ((MainActivity) getActivity()).getSpiceManager().execute(request, new RequestListener<Integer>() {
            @Override
            public void onRequestFailure(SpiceException spiceException) {
                Toast.makeText(getActivity(), "Ошибка при выполнении сетевого запроса", Toast.LENGTH_SHORT).show();
                spiceException.printStackTrace();
            }

            @Override
            public void onRequestSuccess(Integer resultSize) {
                getActivity().setProgressBarIndeterminateVisibility(false);
                if (resultSize == 0) {
                    Toast.makeText(getActivity(), "Не результатов по этому запросу", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
