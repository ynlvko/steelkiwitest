package com.ynlvko.steelkiwitest.network;

import android.content.Context;
import android.net.Uri;

import com.octo.android.robospice.request.SpiceRequest;
import com.ynlvko.steelkiwitest.data.Address;
import com.ynlvko.steelkiwitest.data.SKTContentProvider;
import com.ynlvko.steelkiwitest.utils.Constants;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Locale;

public class SearchRequest extends SpiceRequest<Integer> {
    private Context mContext;
    private String mUrl;

    public SearchRequest(Context context, String url, Class clazz) {
        super(clazz);
        mContext = context;

        Uri.Builder builder = Uri.parse(Constants.BASE_SERVER_URL).buildUpon();
        builder.appendQueryParameter("address", url.replaceAll("\\s", "+"));
        String lang = Locale.getDefault().getLanguage();
        if (lang.equals("uk") || lang.equals("ru")) {
            builder.appendQueryParameter("language", lang);
        }
        mUrl = builder.build().toString();
    }

    @Override
    public Integer loadDataFromNetwork() throws Exception {
        HttpURLConnection urlConnection = (HttpURLConnection) new URL(mUrl).openConnection();
        String result = IOUtils.toString(urlConnection.getInputStream());
        urlConnection.disconnect();

        mContext.getContentResolver().delete(SKTContentProvider.CONTENT_ADDRESS_URI, null, null);
        JSONArray jsonArray = new JSONObject(result).getJSONArray("results");
        Address.putToDb(mContext, jsonArray);

        return jsonArray.length();
    }
}
