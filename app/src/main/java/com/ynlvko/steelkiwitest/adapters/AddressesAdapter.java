package com.ynlvko.steelkiwitest.adapters;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ynlvko.steelkiwitest.data.Address;
import com.ynlvko.steelkiwitest.utils.Utils;

public class AddressesAdapter extends CursorAdapter {

    public AddressesAdapter(Context context) {
        super(context, null, true);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        return new TextView(context);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        view.setBackgroundColor(
                cursor.getPosition() % 2 == 0 ?
                        context.getResources().getColor(android.R.color.white) :
                        context.getResources().getColor(android.R.color.black));
        ((TextView) view).setTextColor(
                cursor.getPosition() % 2 == 0 ?
                        context.getResources().getColor(android.R.color.black) :
                        context.getResources().getColor(android.R.color.white));
        ((TextView) view).setText(Utils.getString(cursor, Address.COLUMN_ADDRESS));
    }
}
